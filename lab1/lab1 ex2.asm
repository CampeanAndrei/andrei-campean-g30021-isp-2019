
_main:
	LDI        R27, 255
	OUT        SPL+0, R27
	LDI        R27, 0
	OUT        SPL+1, R27
	IN         R28, SPL+0
	IN         R29, SPL+1
	SBIW       R28, 1
	OUT        SPL+0, R28
	OUT        SPL+1, R29
	ADIW       R28, 1

;lab1 ex2.c,1 :: 		void main() {
;lab1 ex2.c,3 :: 		DDRB&=~(1<<2);
	IN         R16, DDRB+0
	ANDI       R16, 251
	OUT        DDRB+0, R16
;lab1 ex2.c,4 :: 		DDRB|=(1<<6);
	ORI        R16, 64
	OUT        DDRB+0, R16
;lab1 ex2.c,6 :: 		while(1)
L_main0:
;lab1 ex2.c,8 :: 		if(PINB&(1<<2))
	IN         R16, PINB+0
	SBRS       R16, 2
	JMP        L_main2
;lab1 ex2.c,10 :: 		if(stareaPB2 == 0)
	LDD        R16, Y+0
	CPI        R16, 0
	BREQ       L__main6
	JMP        L_main3
L__main6:
;lab1 ex2.c,12 :: 		stareaPB2=1;
	LDI        R27, 1
	STD        Y+0, R27
;lab1 ex2.c,13 :: 		PORTB^=(1<<6);
	IN         R16, PORTB+0
	LDI        R27, 64
	EOR        R16, R27
	OUT        PORTB+0, R16
;lab1 ex2.c,14 :: 		}
L_main3:
;lab1 ex2.c,16 :: 		} else
	JMP        L_main4
L_main2:
;lab1 ex2.c,17 :: 		stareaPB2=0;
	LDI        R27, 0
	STD        Y+0, R27
L_main4:
;lab1 ex2.c,18 :: 		}
	JMP        L_main0
;lab1 ex2.c,19 :: 		}
L_end_main:
L__main_end_loop:
	JMP        L__main_end_loop
; end of _main
