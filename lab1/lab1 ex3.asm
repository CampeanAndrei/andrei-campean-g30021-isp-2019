
_main:
	LDI        R27, 255
	OUT        SPL+0, R27
	LDI        R27, 0
	OUT        SPL+1, R27
	IN         R28, SPL+0
	IN         R29, SPL+1
	SBIW       R28, 1
	OUT        SPL+0, R28
	OUT        SPL+1, R29
	ADIW       R28, 1

;lab1 ex3.c,1 :: 		void main() {
;lab1 ex3.c,2 :: 		char stareapb2,n=0;
; n start address is: 17 (R17)
	LDI        R17, 0
;lab1 ex3.c,3 :: 		DDRB|=0b11111111;
	IN         R16, DDRB+0
	ORI        R16, 255
	OUT        DDRB+0, R16
;lab1 ex3.c,4 :: 		DDRB&=~(1<<2);
	ANDI       R16, 251
	OUT        DDRB+0, R16
; n end address is: 17 (R17)
;lab1 ex3.c,5 :: 		while(1)
L_main0:
;lab1 ex3.c,7 :: 		if(PINB & (1<<2))
; n start address is: 17 (R17)
	IN         R16, PINB+0
	SBRS       R16, 2
	JMP        L_main2
;lab1 ex3.c,8 :: 		{       if(stareapb2 == 0)
	LDD        R16, Y+0
	CPI        R16, 0
	BREQ       L__main7
	JMP        L__main5
L__main7:
;lab1 ex3.c,10 :: 		stareapb2=1;
	LDI        R27, 1
	STD        Y+0, R27
;lab1 ex3.c,11 :: 		n++;
	MOV        R16, R17
	SUBI       R16, 255
	MOV        R17, R16
; n end address is: 17 (R17)
	MOV        R16, R17
;lab1 ex3.c,12 :: 		}
	JMP        L_main3
L__main5:
;lab1 ex3.c,8 :: 		{       if(stareapb2 == 0)
	MOV        R16, R17
;lab1 ex3.c,12 :: 		}
L_main3:
;lab1 ex3.c,13 :: 		}else
; n start address is: 16 (R16)
	MOV        R17, R16
; n end address is: 16 (R16)
	JMP        L_main4
L_main2:
;lab1 ex3.c,14 :: 		stareapb2=0;
; n start address is: 17 (R17)
	LDI        R27, 0
	STD        Y+0, R27
; n end address is: 17 (R17)
L_main4:
;lab1 ex3.c,15 :: 		PORTD=n;
; n start address is: 17 (R17)
	OUT        PORTD+0, R17
;lab1 ex3.c,16 :: 		}
; n end address is: 17 (R17)
	JMP        L_main0
;lab1 ex3.c,17 :: 		}
L_end_main:
L__main_end_loop:
	JMP        L__main_end_loop
; end of _main
