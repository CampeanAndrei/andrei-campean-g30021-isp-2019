
_afisare:

;lab2 ex2.c,1 :: 		void afisare(char p, char c)
;lab2 ex2.c,3 :: 		PORTA&=0b11110000;
	IN         R16, PORTA+0
	ANDI       R16, 240
	OUT        PORTA+0, R16
;lab2 ex2.c,4 :: 		PORTC&=0b00000000;
	IN         R16, PORTC+0
	ANDI       R16, 0
	OUT        PORTC+0, R16
;lab2 ex2.c,5 :: 		switch(c)
	JMP        L_afisare0
;lab2 ex2.c,7 :: 		case 0:
L_afisare2:
;lab2 ex2.c,8 :: 		PORTC|=0b00111111;break;
	IN         R16, PORTC+0
	ORI        R16, 63
	OUT        PORTC+0, R16
	JMP        L_afisare1
;lab2 ex2.c,9 :: 		case 1:
L_afisare3:
;lab2 ex2.c,10 :: 		PORTC|=0b00000110;break;
	IN         R16, PORTC+0
	ORI        R16, 6
	OUT        PORTC+0, R16
	JMP        L_afisare1
;lab2 ex2.c,11 :: 		case 2:
L_afisare4:
;lab2 ex2.c,12 :: 		PORTC|=0b01011011;break;
	IN         R16, PORTC+0
	ORI        R16, 91
	OUT        PORTC+0, R16
	JMP        L_afisare1
;lab2 ex2.c,13 :: 		case 3:
L_afisare5:
;lab2 ex2.c,14 :: 		PORTC|=0b01001111;break;
	IN         R16, PORTC+0
	ORI        R16, 79
	OUT        PORTC+0, R16
	JMP        L_afisare1
;lab2 ex2.c,15 :: 		case 4:
L_afisare6:
;lab2 ex2.c,16 :: 		PORTC|=0b01100110;break;
	IN         R16, PORTC+0
	ORI        R16, 102
	OUT        PORTC+0, R16
	JMP        L_afisare1
;lab2 ex2.c,17 :: 		case 5:
L_afisare7:
;lab2 ex2.c,18 :: 		PORTC|=0b01001001;break;
	IN         R16, PORTC+0
	ORI        R16, 73
	OUT        PORTC+0, R16
	JMP        L_afisare1
;lab2 ex2.c,19 :: 		case 6:
L_afisare8:
;lab2 ex2.c,20 :: 		PORTC|=0b01111101;break;
	IN         R16, PORTC+0
	ORI        R16, 125
	OUT        PORTC+0, R16
	JMP        L_afisare1
;lab2 ex2.c,21 :: 		case 7:
L_afisare9:
;lab2 ex2.c,22 :: 		PORTC|=0b00000111;break;
	IN         R16, PORTC+0
	ORI        R16, 7
	OUT        PORTC+0, R16
	JMP        L_afisare1
;lab2 ex2.c,23 :: 		case 8:
L_afisare10:
;lab2 ex2.c,24 :: 		PORTC|=0b01111111;break;
	IN         R16, PORTC+0
	ORI        R16, 127
	OUT        PORTC+0, R16
	JMP        L_afisare1
;lab2 ex2.c,25 :: 		case 9:
L_afisare11:
;lab2 ex2.c,26 :: 		PORTC|=0b01101111;break;
	IN         R16, PORTC+0
	ORI        R16, 111
	OUT        PORTC+0, R16
	JMP        L_afisare1
;lab2 ex2.c,27 :: 		}
L_afisare0:
	LDI        R27, 0
	CP         R3, R27
	BRNE       L__afisare23
	JMP        L_afisare2
L__afisare23:
	LDI        R27, 1
	CP         R3, R27
	BRNE       L__afisare24
	JMP        L_afisare3
L__afisare24:
	LDI        R27, 2
	CP         R3, R27
	BRNE       L__afisare25
	JMP        L_afisare4
L__afisare25:
	LDI        R27, 3
	CP         R3, R27
	BRNE       L__afisare26
	JMP        L_afisare5
L__afisare26:
	LDI        R27, 4
	CP         R3, R27
	BRNE       L__afisare27
	JMP        L_afisare6
L__afisare27:
	LDI        R27, 5
	CP         R3, R27
	BRNE       L__afisare28
	JMP        L_afisare7
L__afisare28:
	LDI        R27, 6
	CP         R3, R27
	BRNE       L__afisare29
	JMP        L_afisare8
L__afisare29:
	LDI        R27, 7
	CP         R3, R27
	BRNE       L__afisare30
	JMP        L_afisare9
L__afisare30:
	LDI        R27, 8
	CP         R3, R27
	BRNE       L__afisare31
	JMP        L_afisare10
L__afisare31:
	LDI        R27, 9
	CP         R3, R27
	BRNE       L__afisare32
	JMP        L_afisare11
L__afisare32:
L_afisare1:
;lab2 ex2.c,28 :: 		switch(p)
	JMP        L_afisare12
;lab2 ex2.c,30 :: 		case 1:
L_afisare14:
;lab2 ex2.c,31 :: 		PORTA=0b00001000;break;
	LDI        R27, 8
	OUT        PORTA+0, R27
	JMP        L_afisare13
;lab2 ex2.c,32 :: 		case 2:
L_afisare15:
;lab2 ex2.c,33 :: 		PORTA=0b00000100;break;
	LDI        R27, 4
	OUT        PORTA+0, R27
	JMP        L_afisare13
;lab2 ex2.c,34 :: 		case 3:
L_afisare16:
;lab2 ex2.c,35 :: 		PORTA=0b00000010;break;
	LDI        R27, 2
	OUT        PORTA+0, R27
	JMP        L_afisare13
;lab2 ex2.c,36 :: 		case 4:
L_afisare17:
;lab2 ex2.c,37 :: 		PORTA=0b00000001;break;
	LDI        R27, 1
	OUT        PORTA+0, R27
	JMP        L_afisare13
;lab2 ex2.c,38 :: 		}
L_afisare12:
	LDI        R27, 1
	CP         R2, R27
	BRNE       L__afisare33
	JMP        L_afisare14
L__afisare33:
	LDI        R27, 2
	CP         R2, R27
	BRNE       L__afisare34
	JMP        L_afisare15
L__afisare34:
	LDI        R27, 3
	CP         R2, R27
	BRNE       L__afisare35
	JMP        L_afisare16
L__afisare35:
	LDI        R27, 4
	CP         R2, R27
	BRNE       L__afisare36
	JMP        L_afisare17
L__afisare36:
L_afisare13:
;lab2 ex2.c,39 :: 		Delay_ms(2);
	LDI        R17, 26
	LDI        R16, 249
L_afisare18:
	DEC        R16
	BRNE       L_afisare18
	DEC        R17
	BRNE       L_afisare18
;lab2 ex2.c,40 :: 		}
L_end_afisare:
	RET
; end of _afisare

_main:
	LDI        R27, 255
	OUT        SPL+0, R27
	LDI        R27, 0
	OUT        SPL+1, R27
	IN         R28, SPL+0
	IN         R29, SPL+1
	SBIW       R28, 2
	OUT        SPL+0, R28
	OUT        SPL+1, R29
	ADIW       R28, 1

;lab2 ex2.c,41 :: 		void main() {
;lab2 ex2.c,42 :: 		int value=1749;
	PUSH       R2
	PUSH       R3
	LDI        R27, 213
	STD        Y+0, R27
	LDI        R27, 6
	STD        Y+1, R27
;lab2 ex2.c,43 :: 		DDRA|=0b00001111;
	IN         R16, DDRA+0
	ORI        R16, 15
	OUT        DDRA+0, R16
;lab2 ex2.c,44 :: 		DDRC|=0b11111111;
	IN         R16, DDRC+0
	ORI        R16, 255
	OUT        DDRC+0, R16
;lab2 ex2.c,45 :: 		while(1)
L_main20:
;lab2 ex2.c,47 :: 		afisare(4, value%10);
	LDI        R20, 10
	LDI        R21, 0
	LDD        R16, Y+0
	LDD        R17, Y+1
	CALL       _Div_16x16_S+0
	MOVW       R16, R24
	MOV        R3, R16
	LDI        R27, 4
	MOV        R2, R27
	CALL       _afisare+0
;lab2 ex2.c,48 :: 		afisare(3,(value/10)%10);
	LDI        R20, 10
	LDI        R21, 0
	LDD        R16, Y+0
	LDD        R17, Y+1
	CALL       _Div_16x16_S+0
	MOVW       R16, R22
	LDI        R20, 10
	LDI        R21, 0
	CALL       _Div_16x16_S+0
	MOVW       R16, R24
	MOV        R3, R16
	LDI        R27, 3
	MOV        R2, R27
	CALL       _afisare+0
;lab2 ex2.c,49 :: 		afisare(2,(value/100)%10);
	LDI        R20, 100
	LDI        R21, 0
	LDD        R16, Y+0
	LDD        R17, Y+1
	CALL       _Div_16x16_S+0
	MOVW       R16, R22
	LDI        R20, 10
	LDI        R21, 0
	CALL       _Div_16x16_S+0
	MOVW       R16, R24
	MOV        R3, R16
	LDI        R27, 2
	MOV        R2, R27
	CALL       _afisare+0
;lab2 ex2.c,50 :: 		afisare(1,(value/1000)%10);
	LDI        R20, 232
	LDI        R21, 3
	LDD        R16, Y+0
	LDD        R17, Y+1
	CALL       _Div_16x16_S+0
	MOVW       R16, R22
	LDI        R20, 10
	LDI        R21, 0
	CALL       _Div_16x16_S+0
	MOVW       R16, R24
	MOV        R3, R16
	LDI        R27, 1
	MOV        R2, R27
	CALL       _afisare+0
;lab2 ex2.c,51 :: 		}
	JMP        L_main20
;lab2 ex2.c,52 :: 		}
L_end_main:
	POP        R3
	POP        R2
L__main_end_loop:
	JMP        L__main_end_loop
; end of _main
