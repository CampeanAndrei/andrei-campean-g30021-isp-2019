
_afisare:

;lab2 ex4.c,1 :: 		void afisare(char p, char c)
;lab2 ex4.c,3 :: 		PORTA&=0b11110000;
	IN         R16, PORTA+0
	ANDI       R16, 240
	OUT        PORTA+0, R16
;lab2 ex4.c,4 :: 		PORTC&=0b00000000;
	IN         R16, PORTC+0
	ANDI       R16, 0
	OUT        PORTC+0, R16
;lab2 ex4.c,5 :: 		switch(c)
	JMP        L_afisare0
;lab2 ex4.c,7 :: 		case 0:
L_afisare2:
;lab2 ex4.c,8 :: 		PORTC|=0b00111111;break;
	IN         R16, PORTC+0
	ORI        R16, 63
	OUT        PORTC+0, R16
	JMP        L_afisare1
;lab2 ex4.c,9 :: 		case 1:
L_afisare3:
;lab2 ex4.c,10 :: 		PORTC|=0b00000110;break;
	IN         R16, PORTC+0
	ORI        R16, 6
	OUT        PORTC+0, R16
	JMP        L_afisare1
;lab2 ex4.c,11 :: 		case 2:
L_afisare4:
;lab2 ex4.c,12 :: 		PORTC|=0b01011011;break;
	IN         R16, PORTC+0
	ORI        R16, 91
	OUT        PORTC+0, R16
	JMP        L_afisare1
;lab2 ex4.c,13 :: 		case 3:
L_afisare5:
;lab2 ex4.c,14 :: 		PORTC|=0b01001111;break;
	IN         R16, PORTC+0
	ORI        R16, 79
	OUT        PORTC+0, R16
	JMP        L_afisare1
;lab2 ex4.c,15 :: 		case 4:
L_afisare6:
;lab2 ex4.c,16 :: 		PORTC|=0b01100110;break;
	IN         R16, PORTC+0
	ORI        R16, 102
	OUT        PORTC+0, R16
	JMP        L_afisare1
;lab2 ex4.c,17 :: 		case 5:
L_afisare7:
;lab2 ex4.c,18 :: 		PORTC|=0b01101101;break;
	IN         R16, PORTC+0
	ORI        R16, 109
	OUT        PORTC+0, R16
	JMP        L_afisare1
;lab2 ex4.c,19 :: 		case 6:
L_afisare8:
;lab2 ex4.c,20 :: 		PORTC|=0b01111101;break;
	IN         R16, PORTC+0
	ORI        R16, 125
	OUT        PORTC+0, R16
	JMP        L_afisare1
;lab2 ex4.c,21 :: 		case 7:
L_afisare9:
;lab2 ex4.c,22 :: 		PORTC|=0b00000111;break;
	IN         R16, PORTC+0
	ORI        R16, 7
	OUT        PORTC+0, R16
	JMP        L_afisare1
;lab2 ex4.c,23 :: 		case 8:
L_afisare10:
;lab2 ex4.c,24 :: 		PORTC|=0b01111111;break;
	IN         R16, PORTC+0
	ORI        R16, 127
	OUT        PORTC+0, R16
	JMP        L_afisare1
;lab2 ex4.c,25 :: 		case 9:
L_afisare11:
;lab2 ex4.c,26 :: 		PORTC|=0b01101111;break;
	IN         R16, PORTC+0
	ORI        R16, 111
	OUT        PORTC+0, R16
	JMP        L_afisare1
;lab2 ex4.c,27 :: 		}
L_afisare0:
	LDI        R27, 0
	CP         R3, R27
	BRNE       L__afisare33
	JMP        L_afisare2
L__afisare33:
	LDI        R27, 1
	CP         R3, R27
	BRNE       L__afisare34
	JMP        L_afisare3
L__afisare34:
	LDI        R27, 2
	CP         R3, R27
	BRNE       L__afisare35
	JMP        L_afisare4
L__afisare35:
	LDI        R27, 3
	CP         R3, R27
	BRNE       L__afisare36
	JMP        L_afisare5
L__afisare36:
	LDI        R27, 4
	CP         R3, R27
	BRNE       L__afisare37
	JMP        L_afisare6
L__afisare37:
	LDI        R27, 5
	CP         R3, R27
	BRNE       L__afisare38
	JMP        L_afisare7
L__afisare38:
	LDI        R27, 6
	CP         R3, R27
	BRNE       L__afisare39
	JMP        L_afisare8
L__afisare39:
	LDI        R27, 7
	CP         R3, R27
	BRNE       L__afisare40
	JMP        L_afisare9
L__afisare40:
	LDI        R27, 8
	CP         R3, R27
	BRNE       L__afisare41
	JMP        L_afisare10
L__afisare41:
	LDI        R27, 9
	CP         R3, R27
	BRNE       L__afisare42
	JMP        L_afisare11
L__afisare42:
L_afisare1:
;lab2 ex4.c,28 :: 		switch(p)
	JMP        L_afisare12
;lab2 ex4.c,30 :: 		case 1:
L_afisare14:
;lab2 ex4.c,31 :: 		PORTA=0b00001000;break;
	LDI        R27, 8
	OUT        PORTA+0, R27
	JMP        L_afisare13
;lab2 ex4.c,32 :: 		case 2:
L_afisare15:
;lab2 ex4.c,33 :: 		PORTA=0b00000100;break;
	LDI        R27, 4
	OUT        PORTA+0, R27
	JMP        L_afisare13
;lab2 ex4.c,34 :: 		case 3:
L_afisare16:
;lab2 ex4.c,35 :: 		PORTA=0b00000010;break;
	LDI        R27, 2
	OUT        PORTA+0, R27
	JMP        L_afisare13
;lab2 ex4.c,36 :: 		case 4:
L_afisare17:
;lab2 ex4.c,37 :: 		PORTA=0b00000001;break;
	LDI        R27, 1
	OUT        PORTA+0, R27
	JMP        L_afisare13
;lab2 ex4.c,38 :: 		}
L_afisare12:
	LDI        R27, 1
	CP         R2, R27
	BRNE       L__afisare43
	JMP        L_afisare14
L__afisare43:
	LDI        R27, 2
	CP         R2, R27
	BRNE       L__afisare44
	JMP        L_afisare15
L__afisare44:
	LDI        R27, 3
	CP         R2, R27
	BRNE       L__afisare45
	JMP        L_afisare16
L__afisare45:
	LDI        R27, 4
	CP         R2, R27
	BRNE       L__afisare46
	JMP        L_afisare17
L__afisare46:
L_afisare13:
;lab2 ex4.c,39 :: 		Delay_ms(2);
	LDI        R17, 26
	LDI        R16, 249
L_afisare18:
	DEC        R16
	BRNE       L_afisare18
	DEC        R17
	BRNE       L_afisare18
;lab2 ex4.c,40 :: 		}
L_end_afisare:
	RET
; end of _afisare

_main:
	LDI        R27, 255
	OUT        SPL+0, R27
	LDI        R27, 0
	OUT        SPL+1, R27
	IN         R28, SPL+0
	IN         R29, SPL+1
	SBIW       R28, 4
	OUT        SPL+0, R28
	OUT        SPL+1, R29
	ADIW       R28, 1

;lab2 ex4.c,41 :: 		void main() {
;lab2 ex4.c,42 :: 		int n=0;
	PUSH       R2
	PUSH       R3
	LDI        R27, 0
	STD        Y+2, R27
	STD        Y+3, R27
;lab2 ex4.c,44 :: 		DDRA|=0b00001111;
	IN         R16, DDRA+0
	MOV        R17, R16
	ORI        R17, 15
	OUT        DDRA+0, R17
;lab2 ex4.c,45 :: 		DDRC|=0b11111111;
	IN         R16, DDRC+0
	ORI        R16, 255
	OUT        DDRC+0, R16
;lab2 ex4.c,46 :: 		DDRA&=~(1<<7);
	MOV        R16, R17
	ANDI       R16, 127
	OUT        DDRA+0, R16
;lab2 ex4.c,47 :: 		DDRA&=~(1<<6);
	ANDI       R16, 191
	OUT        DDRA+0, R16
;lab2 ex4.c,48 :: 		while(1)
L_main20:
;lab2 ex4.c,50 :: 		if(PINA & (1<<7))
	IN         R16, PINA+0
	SBRS       R16, 7
	JMP        L_main22
;lab2 ex4.c,51 :: 		{       if(stareapa7 == 0)
	LDD        R16, Y+0
	CPI        R16, 0
	BREQ       L__main48
	JMP        L_main23
L__main48:
;lab2 ex4.c,53 :: 		stareapa7=1;
	LDI        R27, 1
	STD        Y+0, R27
;lab2 ex4.c,54 :: 		if(PINA & (1<<6))
	IN         R16, PINA+0
	SBRS       R16, 6
	JMP        L_main24
;lab2 ex4.c,55 :: 		n++;
	LDD        R16, Y+2
	LDD        R17, Y+3
	SUBI       R16, 255
	SBCI       R17, 255
	STD        Y+2, R16
	STD        Y+3, R17
	JMP        L_main25
L_main24:
;lab2 ex4.c,57 :: 		n--;
	LDD        R16, Y+2
	LDD        R17, Y+3
	SUBI       R16, 1
	SBCI       R17, 0
	STD        Y+2, R16
	STD        Y+3, R17
L_main25:
;lab2 ex4.c,58 :: 		}
L_main23:
;lab2 ex4.c,59 :: 		}else
	JMP        L_main26
L_main22:
;lab2 ex4.c,60 :: 		stareapa7=0;
	LDI        R27, 0
	STD        Y+0, R27
L_main26:
;lab2 ex4.c,61 :: 		if(PINA & (1<<6))
	IN         R16, PINA+0
	SBRS       R16, 6
	JMP        L_main27
;lab2 ex4.c,62 :: 		{       if(stareapa6 == 0)
	LDD        R16, Y+1
	CPI        R16, 0
	BREQ       L__main49
	JMP        L_main28
L__main49:
;lab2 ex4.c,64 :: 		stareapa6=1;
	LDI        R27, 1
	STD        Y+1, R27
;lab2 ex4.c,65 :: 		if(n==0)
	LDD        R16, Y+2
	LDD        R17, Y+3
	CPI        R17, 0
	BRNE       L__main50
	CPI        R16, 0
L__main50:
	BREQ       L__main51
	JMP        L_main29
L__main51:
;lab2 ex4.c,66 :: 		n=9999;
	LDI        R27, 15
	STD        Y+2, R27
	LDI        R27, 39
	STD        Y+3, R27
	JMP        L_main30
L_main29:
;lab2 ex4.c,68 :: 		n--;
	LDD        R16, Y+2
	LDD        R17, Y+3
	SUBI       R16, 1
	SBCI       R17, 0
	STD        Y+2, R16
	STD        Y+3, R17
L_main30:
;lab2 ex4.c,69 :: 		}
L_main28:
;lab2 ex4.c,70 :: 		}else
	JMP        L_main31
L_main27:
;lab2 ex4.c,71 :: 		stareapa6=0;
	LDI        R27, 0
	STD        Y+1, R27
L_main31:
;lab2 ex4.c,72 :: 		afisare(4, n%10);
	LDI        R20, 10
	LDI        R21, 0
	LDD        R16, Y+2
	LDD        R17, Y+3
	CALL       _Div_16x16_S+0
	MOVW       R16, R24
	MOV        R3, R16
	LDI        R27, 4
	MOV        R2, R27
	CALL       _afisare+0
;lab2 ex4.c,73 :: 		afisare(3,(n/10)%10);
	LDI        R20, 10
	LDI        R21, 0
	LDD        R16, Y+2
	LDD        R17, Y+3
	CALL       _Div_16x16_S+0
	MOVW       R16, R22
	LDI        R20, 10
	LDI        R21, 0
	CALL       _Div_16x16_S+0
	MOVW       R16, R24
	MOV        R3, R16
	LDI        R27, 3
	MOV        R2, R27
	CALL       _afisare+0
;lab2 ex4.c,74 :: 		afisare(2,(n/100)%10);
	LDI        R20, 100
	LDI        R21, 0
	LDD        R16, Y+2
	LDD        R17, Y+3
	CALL       _Div_16x16_S+0
	MOVW       R16, R22
	LDI        R20, 10
	LDI        R21, 0
	CALL       _Div_16x16_S+0
	MOVW       R16, R24
	MOV        R3, R16
	LDI        R27, 2
	MOV        R2, R27
	CALL       _afisare+0
;lab2 ex4.c,75 :: 		afisare(1,(n/1000)%10);
	LDI        R20, 232
	LDI        R21, 3
	LDD        R16, Y+2
	LDD        R17, Y+3
	CALL       _Div_16x16_S+0
	MOVW       R16, R22
	LDI        R20, 10
	LDI        R21, 0
	CALL       _Div_16x16_S+0
	MOVW       R16, R24
	MOV        R3, R16
	LDI        R27, 1
	MOV        R2, R27
	CALL       _afisare+0
;lab2 ex4.c,82 :: 		}
	JMP        L_main20
;lab2 ex4.c,83 :: 		}
L_end_main:
	POP        R3
	POP        R2
L__main_end_loop:
	JMP        L__main_end_loop
; end of _main
