
_main:
	LDI        R27, 255
	OUT        SPL+0, R27
	LDI        R27, 0
	OUT        SPL+1, R27

;lab2 ex1.c,1 :: 		void main() {
;lab2 ex1.c,2 :: 		DDRA|=0b00001111;
	IN         R16, DDRA+0
	ORI        R16, 15
	OUT        DDRA+0, R16
;lab2 ex1.c,3 :: 		DDRC|=0b11111111;
	IN         R16, DDRC+0
	ORI        R16, 255
	OUT        DDRC+0, R16
;lab2 ex1.c,4 :: 		while(1)
L_main0:
;lab2 ex1.c,6 :: 		PORTA=0b00001000;
	LDI        R27, 8
	OUT        PORTA+0, R27
;lab2 ex1.c,7 :: 		PORTC=0b00000110;
	LDI        R27, 6
	OUT        PORTC+0, R27
;lab2 ex1.c,8 :: 		Delay_ms(1);
	LDI        R17, 13
	LDI        R16, 252
L_main2:
	DEC        R16
	BRNE       L_main2
	DEC        R17
	BRNE       L_main2
	NOP
;lab2 ex1.c,9 :: 		PORTA=0b00000100;
	LDI        R27, 4
	OUT        PORTA+0, R27
;lab2 ex1.c,10 :: 		PORTC=0b01011011;
	LDI        R27, 91
	OUT        PORTC+0, R27
;lab2 ex1.c,11 :: 		Delay_ms(1);
	LDI        R17, 13
	LDI        R16, 252
L_main4:
	DEC        R16
	BRNE       L_main4
	DEC        R17
	BRNE       L_main4
	NOP
;lab2 ex1.c,12 :: 		PORTA=0b00000010;
	LDI        R27, 2
	OUT        PORTA+0, R27
;lab2 ex1.c,13 :: 		PORTC=0b01001111;
	LDI        R27, 79
	OUT        PORTC+0, R27
;lab2 ex1.c,14 :: 		Delay_ms(1);
	LDI        R17, 13
	LDI        R16, 252
L_main6:
	DEC        R16
	BRNE       L_main6
	DEC        R17
	BRNE       L_main6
	NOP
;lab2 ex1.c,15 :: 		PORTA=0b00000001;
	LDI        R27, 1
	OUT        PORTA+0, R27
;lab2 ex1.c,16 :: 		PORTC=0b01100110;
	LDI        R27, 102
	OUT        PORTC+0, R27
;lab2 ex1.c,17 :: 		Delay_ms(1);
	LDI        R17, 13
	LDI        R16, 252
L_main8:
	DEC        R16
	BRNE       L_main8
	DEC        R17
	BRNE       L_main8
	NOP
;lab2 ex1.c,18 :: 		}
	JMP        L_main0
;lab2 ex1.c,19 :: 		}
L_end_main:
L__main_end_loop:
	JMP        L__main_end_loop
; end of _main
